package com.hnr.springmsautheurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class SpringMsAuthEurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMsAuthEurekaServerApplication.class, args);
	}
}
